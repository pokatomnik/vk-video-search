#!/usr/bin/env python3

from urllib.parse import urlencode, quote
from urllib.request import Request, urlopen
import urllib.request
import urllib
from socket import timeout
from pyquery import PyQuery as pq
import sys
import re
import json
import time
import cgi
import html
import datetime
import random
import string

form = cgi.FieldStorage()
search_string = html.escape(form.getfirst("search_string", "скоро, очень скоро"))
pages_num = int(html.escape(form.getfirst("pages_number", "1")))
print("Content-Type: application/json\n")
# print("Content-Type: text/html\n\n")


settings = {
	'vk_api_key' : '8d4947a93b74731fc5ef6dfe87b3281858b44ba53b6ee1b293cf7125710e8c7904e15646fec8215d7f99e',
	'vk_friends_request_fields' : [
		'uid',
		'first_name',
		'last_name',
		'nickname',
		'sex',
		'bdate',
		'city',
		'country',
		'timezone',
		'photo',
		'photo_medium',
		'photo_big',
		'domain',
		'has_mobile',
		'rate',
		'contacts',
		'education'
	],
	'vk_user_request_fields' : [
		'photo_id',
		'verified',
		'sex',
		'bdate',
		'city',
		'country',
		'home_town',
		'has_photo',
		'photo_max_orig',
		'domain',
		'has_mobile',
		'contacts',
		'site',
		'education',
		'universities',
		'schools',
		'status',
		'last_seen',
		'followers_count',
		'common_count',
		'occupation',
		'nickname',
		'relatives',
		'relation',
		'personal',
		'connections',
		'exports',
		'wall_comments',
		'activities',
		'interests',
		'music',
		'movies',
		'tv',
		'books',
		'games',
		'about',
		'quotes',
		'timezone',
		'screen_name',
		'maiden_name',
		'career',
		'military'
	],
	'known_cities' : {
		'chelyabinsk' : {
			'title' : 'Челябинск',
			'id' : 158
		},
		'moscow' : {
			'title' : 'Москва',
			'id' : 1
		},
		'spb' : {
			'title' : 'Санкт-Петербург',
			'id' : 2
		}
	}
}

# qs - type: <class 'str'> string for search
# num - type : <class 'int'> number of pages to search
def google_search(qs, num):

	def get_random_ua():
		return [
			'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
			'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko',
			'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
			'Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20121202 Firefox/17.0 Iceweasel/17.0.1'
		][random.randint(0, 4)]

	search_q = quote(qs+' site:vk.com')
	search_n = num if num < 10 else 10
	# print(search_q, search_n)

	search_qs = ["https://google.com/search?q={0}&tbm=vid&start={1}".format(search_q, i*10) for i in range(0, search_n)]
	
	links = []
	for q in search_qs:
		successful = False
		while not successful:
			try:
				ua = get_random_ua()
				time_to_sleep = random.randint(10, 40)

				# debug
				# print('Wait {0} seconds, search with ua: {1}'.format(time_to_sleep, ua))

				time.sleep(time_to_sleep)
				# debug
				# print('Fetching url: {0}'.format(q))

				# now searching
				request = urllib.request.Request(
					q,
					data=None,
					headers={
						'User-Agent' : ua
					}
				)
				res = urllib.request.urlopen(request, timeout=30).read().decode('utf-8')
				d = pq(res)
				a_s = d('.g .rc .r a')
				links = links + [d(a).attr('href') for a in a_s]
				# debug	
				# print('Links fetched : {0}'.format(len(links)))
				successful = True

			except urllib.error.URLError as e:
				console.log(e.read().decode('utf-8'))
				successful = True
			except timeout:
				pass
			except:
				successful = True
			finally:
				pass

	return links

def make_get_for_search(url):
	html = re.search('<ol.{1,}\/ol>', urlopen(url).read().decode('utf-8'))
	if (html != None):
		return html.group(0)
	else:
		return None

def make_get_for_vk_page_query(url):
	html = re.search('<dl class="si_row.{1,}\/dl', urlopen(url).read().decode('utf-8'))
	if (html != None):
		return html.group(0)
	else:
		return None

def page_num_to_query_param(num):
	return 10*(num-1)+1

def get_results_from_page(search_string, num):
	url = ('http://www.bing.com/search?q={0}+site'+quote(':')+'vk.com&qs=n&pq={1}&sc=8-3&sp=-1&sk=&FORM=PERE&first={2}').format(
		quote(search_string), quote(search_string), str(page_num_to_query_param(num))
	)
	# debug
	# print(url)
	html = make_get_for_search(url)

	d = pq(html)
	set_of_a_elements = d('li.b_algo h2 a')

	result = []

	for a_element in set_of_a_elements:
		result.append(pq(a_element).attr('href'))
		
	if (len(set_of_a_elements) == 0):
		f = open('error.html', 'w');
		f.write(html)
		f.close()
		# debug
		# print('Unable to parse response. See html in error.html');
		sys.exit(1)
	return result


# fetch all results in search
# all_results = []
# for page in range(1, pages_num+1):
# 	# debug
# 	# print('Page : %s' % page)
# 	all_results += get_results_from_page(search_string, int(page))

# 	# if results has not fetched, try again
# 	while (len(all_results) == 0):
# 		# debug
# 		# print('Last query failed, repeating')
# 		all_results += get_results_from_page(search_string, int(page))
# debug
#print(len(all_results))
all_results = google_search(search_string, int(pages_num))
# filter only video results
video_results = []

for result in all_results:
	is_video = re.search('\/video.{1,}_[\d]{1,}$', result)

	if (is_video != None):
		video_results.append({'video_href' : result, 'number' : all_results.index(result)+1})

# create links for static pages
for result in video_results:
	result['no_js_page'] = re.sub('^(http\:\/\/vk.com)|^(https\:\/\/vk.com)', 'http://m.vk.com', result['video_href'])

# get sender urls
for result in video_results:
	answer = make_get_for_vk_page_query(result['no_js_page'])
	if (answer != None):
		result['sender_href'] = pq(answer)('a').attr('href')
	else:
		result['sender_href'] = 'None'

# get vk api data
for result in video_results:
	try:
		query_url = 'https://api.vk.com/method/users.get?uids={0}&count=1&fields={1}&access_token={2}'.format(
			result['sender_href'][1:],
			','.join(settings['vk_user_request_fields']),
			settings['vk_api_key']
		)
		answer_raw = urlopen(query_url).read().decode('utf-8')
		answer = json.loads(answer_raw)['response'][0]
		result['vk_data'] = answer
	except:
		pass

# for result in video_results:
# 	try:
# 		query_url = 'https://api.vk.com/method/friends.get?uid={0}&fields={1}&access_token={2}'.format(
# 			result['vk_data']['uid'],
# 			','.join(settings['vk_friends_request_fields']),
# 			settings['vk_api_key']
# 		)
# 		answer = json.loads(urlopen(query_url).read().decode('utf-8'))['response']
# 		result['friend_list'] = answer
# 	except:
# 		pass

table = str.maketrans("", "", "!@#$%^&*_+|+\/:;[]{}<>")
file = open('{0}-{1}.json'.format(search_string.translate(table), str(datetime.datetime.now()).replace(':', '-')), 'w');
file.write(json.dumps(video_results, indent=4, sort_keys=True, ensure_ascii=False));
file.close();
print(json.dumps(video_results))